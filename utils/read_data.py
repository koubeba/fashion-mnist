'''
Module containing all methods and DataSet class required to extract data 
from Fashion MNIST training data
'''


import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import logging

def load_data(path, datatype='train'):
    '''
    Function retrieving byte data from gzipped training data

    :param path: relative or absolute path to training data
    :param datatype: defines which training data will be currently used
        possible: train / t10k
    :returns: images - vector of every image info (784 bites)
              labels - vector of labels for images
    '''
    import os
    import gzip

    lpath = os.path.join(path, '%s-labels-idx1-ubyte.gz' % datatype)
    ipath = os.path.join(path, '%s-images-idx3-ubyte.gz' % datatype)

    try:
        with gzip.open(lpath, 'rb') as lbpath:
            # from file containing labels create vector containing 8bit ints with read values
            labels = np.frombuffer(lbpath.read(), dtype=np.uint8, offset=8)

        with gzip.open(ipath, 'rb') as imgpath:
            # from file containing images create vector made of 784 8bit ints
            images = np.frombuffer(imgpath.read(), dtype=np.uint8, offset=16).reshape(len(labels), 784)

    except FileNotFoundError:
        logging.exception('Selected Files does not exist!')

    return images, labels

def from_vec_to_matrix(vec):
    '''
    Function used for reshaping 1D 784-elem vectors into 2D 28x28 matrixes

    :param vec: vector containing 784-elem vectors
    :returns: vector containing 28x28-elem matrixes
    '''
    return np.reshape(vec, (-1, 28, 28))

def invert_grayscale(vec):
    '''
    Function used for reverting RGB values

    :param vec: vector containing vectors with RGB values
    :returns: vector containing vectors with reversed RGB values
    '''
    return 255 - vec

def create_sprite_image(images):
    '''
    Function used for putting vector of image vectors into square matrix
    to easily represent it in .png file
    
    e.g. from 1x60000 vector of train images create 265x265 matrix

    :param images: vector of image datas
    :returns: square matrix of image datas and size of matrix's side
    '''

    if isinstance(images, list):
        images = np.array(images)

    img_h = images.shape[1]
    img_w = images.shape[2]
    n_plots = int(np.ceil(np.sqrt(images.shape[0])))

    spriteimage = np.ones((img_h * n_plots, img_w * n_plots))

    for i in range(n_plots):
        for j in range(n_plots):
            # calculate index of next image
            this_filter = i * n_plots + j

            # if index of next image is less than size of `images` vector,
            # then put into new matrix image data in exact location
            if this_filter < images.shape[0]:
                this_img = images[this_filter]
                spriteimage[i * img_h:(i + 1) * img_h, j * img_w:(j + 1) * img_w] = this_img

    return spriteimage, n_plots

def get_sprite_image(to_visualise, do_invert=True):
    '''
    Function used for retrieving square matrix of images

    :param to_visualise: vector of image datas
    :param do_invert: True if we want colour-invertion, False otherwise

    :returns: square matrix of image datas
    '''
    to_visualise = from_vec_to_matrix(to_visualise)
    if do_invert:
        to_visualise = invert_grayscale(to_visualise)
    return create_sprite_image(to_visualise)

def vectorize_output(out):
    '''
    Function used to create vector from label value
    '''
    vect = np.zeros(10)
    vect[out] = 1.0
    return vect

class DataSet(object):
    '''
    Class defining object used for holding neural network data

    :arg datatype: defines if hold data is for training or testing
    :arg X: vector of image data
    :arg Y: vector of label data
    :arg X_sprite: square matrix of image data
    :arg X_sprite_plots: size of X_sprite matrix's side
    '''

    def __init__(self, path, datatype):
        self.datatype = datatype
        self.X, self.Y = load_data(path, datatype)
        self.X_sprite, self.X_sprite_plots = get_sprite_image(self.X)

        self.Y_matrix = [vectorize_output(out) for out in self.Y]
        

    def save_full_image(self, name):
        '''
        Function creating .png file from `X_sprite`

        :param name: name of created .png file
        '''
        plt.imsave(name + '.png', self.X_sprite, cmap='gray')

    def print_one_elem_image(self, idx):
        '''
        Function used for displaying exact image by idx

        :param idx: index of image
        '''
        plt.imshow(self.X[idx,].reshape(28,28), cmap= matplotlib.cm.binary)
        plt.axis("off")
        plt.show()

    def get_X_elem(self, idx):
        '''
        Function returning image data from `X`

        :param idx: index of image
        :returns: image data from `X`
        '''
        return self.X[idx]

    def get_X_elem_matrix(self,idx):
        '''
        Function returning image data from `X_sprite`

        returns data inverted to `get_X_elem`

        :param idx: index of image
        :returns: image data from `X_sprite`
        '''
        column = int(np.floor(idx/self.X_sprite_plots))
        idx -= int(column * self.X_sprite_plots)
        return self.X_sprite[column * 28 : (column + 1) * 28, idx * 28 : (idx+1) * 28]

    def get_Y_elem(self, idx):
        '''
        Function returning image data from `Y`

        :param idx: index of image
        :returns: label data from `Y`
        '''
        return self.Y[idx]

    def get_image_vector(self):
        '''
        Getter of `X` vector
        '''
        return self.X

    def get_image_matrix(self):
        '''
        Getter of `X_sprite` vector
        '''
        return self.X_sprite

    def get_labels(self):
        '''
        Getter of `Y` vector
        '''
        return self.Y


    def get_Y_matrix(self):
        '''
        Matrix of output-formatted label vectors
        e.g. labels[1] == 1: y_matrix[1] = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
        '''
        return self.Y_matrix

    def get_Y_i_output(self, idx):
        return self.Y_matrix[idx]


if __name__ == '__main__':
    ds = DataSet('../data/', 'train')
    print(ds.get_image_vector()[0])
    print(ds.get_Y_i_output(0))