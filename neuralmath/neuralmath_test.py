from . import neuralmath
import unittest
import numpy

class TestNodeFunctions(unittest.TestCase):
    def test_linear(self):
        input = 3
        result = neuralmath.linear(input)
        self.assertEqual(result, input)

        input = numpy.array([3, 2])
        result = neuralmath.linear(input)
        self.assertEqual(result.tolist(), input.tolist())

        input = numpy.array([[3, 2], [4, 5]])
        result = neuralmath.linear(input)
        self.assertEqual(result.tolist(), input.tolist())

    def test_linear_prime(self):
        input = 3
        result = neuralmath.linear_prime(input)
        self.assertEqual(result, 1)

        input = numpy.array([3, 2])
        result = neuralmath.linear_prime(input)
        self.assertEqual(result.tolist(), numpy.array([1, 1]).tolist())

        input = numpy.array([[3, 2], [4, 5]])
        result = neuralmath.linear_prime(input)
        self.assertEqual(result.tolist(), numpy.array([[1, 1], [1, 1]]).tolist())

    def test_sigmoid(self):
        input = 0
        result = neuralmath.sigmoid(input)
        self.assertEqual(result, 0.5)

        input = numpy.array([0, 0])
        result = neuralmath.sigmoid(input)
        self.assertEqual(result.tolist(), numpy.array([0.5, 0.5]).tolist())

        input = numpy.array([[0, 0], [0, 0]])
        result = neuralmath.sigmoid(input)
        self.assertEqual(result.tolist(), numpy.array([[0.5, 0.5], [0.5, 0.5]]).tolist())

    def test_sigmoid_prime(self):
        input = 0
        result = neuralmath.sigmoid_prime(input)
        self.assertEqual(result, 0.25)

        input = numpy.array([0, 0])
        result = neuralmath.sigmoid_prime(input)
        self.assertEqual(result.tolist(), numpy.array([0.25, 0.25]).tolist())

        input = numpy.array([[0, 0], [0, 0]])
        result = neuralmath.sigmoid_prime(input)
        self.assertEqual(result.tolist(), numpy.array([[0.25, 0.25], [0.25, 0.25]]).tolist())

    def test_relu(self):
        input = -2
        result = neuralmath.reLU(input)
        self.assertEqual(result, 0)

        input = 0
        result = neuralmath.reLU(input)
        self.assertEqual(result, 0)

        input = 1
        result = neuralmath.reLU(input)
        self.assertEqual(result, 1)

        input = numpy.array([-1, 3])
        result = neuralmath.reLU(input)
        self.assertEqual(result.tolist(), numpy.array([0, 3]).tolist())

    def test_relu_prime(self):
        input = -2
        result = neuralmath.reLU_prime(input)
        self.assertEqual(result, 0)

        input = 2
        result = neuralmath.reLU_prime(input)
        self.assertEqual(result, 1)

        input = numpy.array([-1, 1])
        result = neuralmath.reLU_prime(input)
        self.assertEqual(result.tolist(), numpy.array([0, 1]).tolist())

        input = numpy.array([[-1, 1], [-9, -9]])
        result = neuralmath.reLU_prime(input)
        self.assertEqual(result.tolist(), numpy.array([[0, 1], [0, 0]]).tolist())

    def test_relu_prime(self):
        input = -2
        result = neuralmath.reLU_prime(input)
        self.assertEqual(result, 0)

        input = 2
        result = neuralmath.reLU_prime(input)
        self.assertEqual(result, 1)

    def test_L2_loss(self):
        Y_test = numpy.array([1, 0])
        Y_hat_test = numpy.array([0, 1])
        result = neuralmath.L2_loss(Y_test, Y_hat_test)
        self.assertEqual(result, 2)

        Y_test = numpy.array([1, 1])
        Y_hat_test = numpy.array([1, 1])
        result = neuralmath.L2_loss(Y_test, Y_hat_test)
        self.assertEqual(result, 0)

        Y_test = numpy.array([[1, 0], [1, 0]])
        Y_hat_test = numpy.array([[0.5, 0.25], [0, 1]])
        result = neuralmath.L2_loss(Y_test, Y_hat_test)
        self.assertEqual(result, 2.3125)

    def test_L2_prime(self):
        Y_test = numpy.array([1, 0])
        Y_hat_test = numpy.array([0, 1])
        result = neuralmath.L2_loss_prime(Y_test, Y_hat_test)
        self.assertEqual(result.tolist(), numpy.array([2, -2]).tolist())

        Y_test = numpy.array([1, 1])
        Y_hat_test = numpy.array([1, 1])
        result = neuralmath.L2_loss_prime(Y_test, Y_hat_test)
        self.assertEqual(result.tolist(), numpy.array([0, 0]).tolist())

        Y_test = numpy.array([[1, 0], [1, 0]])
        Y_hat_test = numpy.array([[0.5, 0.25], [0, 1]])
        result = neuralmath.L2_loss_prime(Y_test, Y_hat_test)
        self.assertEqual(result.tolist(), numpy.array([[1, -0.5], [2, -2]]).tolist())

    def test_average(self):
        Y_test = numpy.array([4, 2, 1])
        m = 2
        result = neuralmath.average(Y_test, 2)
        self.assertEqual(result.tolist(), numpy.array([2, 1, 0.5]).tolist())
