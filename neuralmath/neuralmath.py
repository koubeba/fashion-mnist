import numpy as np

# The simplest cost function there is.
# Calculates the difference between each predicted output and the actual value.
# The difference is squared to measure the absolute value of the difference.


def L2_loss(Y, Y_hat):
    """
    L2 (Sum of square differences) cost function.
    :param Y: vector of actual values
    :param Y_hat: vector of predicted output values
    :return: sum of costs
    """
    return np.sum(np.power(np.subtract(Y, Y_hat), 2))


def L2_loss_prime(Y, Y_hat):
    """
    Derivative of L2 loss function with respect to its input values.
    :param Y: vector of actual values
    :param Y_hat: vector of predicted output values
    :return: vector of derivatives
    """
    return 2*np.subtract(Y, Y_hat)


def L1_loss(Y, Y_hat):
    """
    L1 (Sum of absolute differences) cost function.
    :param Y: vector of actual values
    :param Y_hat: vector of predicted output values
    :return: sum of costs
    """
    return np.sum(np.abs(np.subtract(Y, Y_hat)))

def L1_loss_prime(Y, Y_hat):
    """
    Derivative of L1 loss function with respect to its input values.
    :param Y: vector of actual values
    :param Y_hat: vector of predicted output values
    :return: vector of derivatives
    """
    return np.sum(np.sign(np.subtract(Y, Y_hat)))

###################################################
# Node activation functions #######################
###################################################


def linear(z):
    """
    A linear neuron activation function.
    :param z: input argument
    :return: neuron's output value
    """
    return z


def reLU(z):
    """
    A reLU neuron activation function.
    :param z: input argument
    :return: neuron's output value
    """
    if isinstance(z, int) or np.ndim(z) == 0:
        return max(0, z)

    from copy import deepcopy
    y = deepcopy(z)

    y[y<0] = 0
    return y

def sigmoid(z):
    """
    A sigmoid neuron activation function.
    :param z: input argument
    :return: neuron's output value
    """
    return 1 / (1 + np.exp(-z))

def tanh(z):
    """
    A tanh neuron activation function.
    :param z: input argument
    :return: neuron's output value
    """
    return np.tanh(z)


###################################################
# Node activation functions derivatives ###########
###################################################


def linear_prime(z):
    """
    Derivative of linear neuron activation function with respect to input argument.
    :param z: input argument
    :return: neuron's output value
    """
    return np.ones(np.shape(z))


def sigmoid_prime(z):
    """
    Derivative of sigmoid neuron activation function with respect to input argument.
    :param z: input argument
    :return: neuron's output value
    """
    return sigmoid(z)*(1-sigmoid(z))


def reLU_prime(z):
    """
    Derivative of reLU neuron activation function with respect to input argument.
    :param z: input argument
    :return: neuron's output value
    """
    if isinstance(z, int) or np.ndim(z) == 0:
        return 1 if z > 0 else 0

    from copy import deepcopy
    y = deepcopy(z)
    y[y<=0] = 0
    y[y>0] = 1
    return y


def tanh_prime(z):
    """
    Derivative of tanh neuron activation function with respect to input argument.
    :param z: input argument
    :return: neuron's output value
    """
    return 1/np.square(np.cosh(z))

###################################################
# Weight initialization methods ###################
###################################################

def he_initialization(size):
    """
    Computes the He weight initialization multiplier.
    Used for reLU activation function.
    :param size: number of neurons in layer behind the weights
    :return: weight initialization multiplier
    """
    return np.sqrt(2/size)


def xavier_initialization(size):
    """
    Computes the Xavier weight initialization multiplier.
    Used for tanh activation function.
    :param size: number of neurons in layer behind the weights
    :return: weight initialization multiplier
    """
    return np.sqrt(1 / size)


def weight_initialization(size, next_size):
    """
    Computes the weight initialization multiplier.
    :param size: number of neurons in layer behind the weights
    :param next_size: number of neurons in next layer
    :return: weight initialization multiplier
    """
    return np.sqrt(2 / size + next_size)


###################################################
# Helper functions ################################
###################################################


def average(sum, m):
    """
    Average of a cost sum value through m training sets.
    :param sum: cost sum value
    :param m: number of training sets
    :return: averaged cost value
    """
    return (1 / m) * sum
