import numpy as np
from neuralmath import neuralmath

class Layer:
    '''
    Class defining object used as layers in FFNN
    '''

    def __init__(self, layer_size, next_layer_size, activation_function=neuralmath.linear):
        '''
        Initialization function of ``Layer`` object

        Creates vectors of ``activations`` and ``zs``,
        where object of ``zs`` is result of calculating:
        Weights to THIS layer * Previous activations + biases

        and object of ``zs`` is result of calculating:
        return of activation function fed up with vector ``zs``

        :param layer_size: Count of neurons in THIS layer
        :param next_layer_size: Count of neurons in NEXT layer
        '''
        self.neuron_count = layer_size
        self.zs = np.zeros((layer_size, 1))
        self.acts = np.zeros((layer_size, 1))
        self.w_delta = None
        self.b_delta = None
        self.activation_function = activation_function

        

        # initialize weights and biases 
        if next_layer_size != 0:
            self.weights = np.random.normal(0, 0.1, size=(next_layer_size, layer_size))

            # Initialize the weights with optimized values:

            if self.activation_function == neuralmath.reLU:
                self.weights = self.weights * neuralmath.he_initialization(layer_size)
            elif self.activation_function == neuralmath.tanh:
                self.weights = self.weights * neuralmath.xavier_initialization(layer_size)
            else:
                self.weights = self.weights * neuralmath.weight_initialization(layer_size, next_layer_size)

            self.biases = np.random.normal(0, 0.1, size=(next_layer_size, 1))
        # if layer has no next layer, it's output layer
        # we don't need to create arbitrary weights and biases
        else:
            self.weights = None
            self.biases = None

    def __str__(self):
        return str(self.acts)

    def __repr__(self):
        return str(self.acts)

class NeuralNetwork:
    '''
    Class defining FFNN
    '''

    def __init__(self, layer_sizes, 
                    f_activ=neuralmath.reLU,
                    f_activ_prime=neuralmath.reLU_prime,
                    f_loss=neuralmath.L2_loss,
                    f_loss_prime=neuralmath.L2_loss_prime,
                    optimizer='Gradient',
                    layers=None):
        '''
        Initialization function of ``NeuralNetwork`` object

        :param layer_sizes: list of all desired layer neuron counts,
                            e.g. [784, 20, 10] will create NN with
                            3 layers, including 784, 20, 10 neurons
                            respectively
        :param f_activ: reference to activation function
        :param f_activ_prime: reference to derived activation function
        :param f_loss: reference to loss function
        :param f_loss_prime: reference to derived loss function
        :param layers: list of ``Layer`` object, used for loading trained
                    Neural Network data, so we can do testing
        '''
        
        self.layer_count = len(layer_sizes)
        self.layer_sizes = layer_sizes
        self.f_activ = f_activ
        self.f_activ_prime = f_activ_prime
        self.f_loss = f_loss
        self.f_loss_prime = f_loss_prime
        self.optimizer = optimizer
        self.momentum = Momentum(self.layer_count)
        self.adam = Adam(self.layer_count)

        # sum of costs from ``f_loss`` function
        # used for calculating average cost between epochs
        self.cost_sum = 0

        # if no list of ``layer`` object was provided then create new
        if not layers:
            self.layers = []
            for i in range(self.layer_count):
                if i != self.layer_count-1:
                    layer = Layer(layer_sizes[i], layer_sizes[i+1], self.f_activ)
                else:
                    layer = Layer(layer_sizes[i], 0)
                self.layers.append(layer)
        # if list of ``layer`` object was provided just assign it
        # used only from ``Tester`` class
        else:
            self.layers = layers

    def __getitem__(self, idx):
        return self.layers[idx]

    def feed_forward(self, inp):
        '''
        Function used to propagate forward input vector
        in neural network.

        :param inp: vector of image data
        '''

        # first activation always will be input data
        self[0].acts = np.reshape(inp, (self.layer_sizes[0], 1))
        for i in range(1, self.layer_count):
            self[i].zs = np.dot(self[i-1].weights, self[i-1].acts) + \
                        self[i-1].biases
            self[i].acts = self.f_activ(self[i].zs)

    def backpropagate(self, learning_rate, expected):
        '''
        Function used to backpropagate error and correct weights and biases

        :param learning_rate: scale defining how much changes affect 
                            weights and biases
        :param expected: 1x10 vector defining expected output label
        '''
        expected = np.reshape(expected, (self.layer_sizes[-1], 1))

        # backpropagate output layer error
        self[-1].b_delta = self.f_loss_prime(self[-1].acts, expected) * \
            self.f_activ_prime(self[-1].zs)
        self[-1].w_delta = np.dot(self[-1].b_delta, self[-2].acts.T)

        # add new cost to total sum of costs 
        self.cost_sum += self.get_cost(expected)
        
        # backpropagate until the last layer
        for l in range(2, self.layer_count):
            self[-l].b_delta = np.dot(self[-l].weights.T, self[-l+1].b_delta) * \
                self.f_activ_prime(self[-l].zs)
            self[-l].w_delta = np.dot(self[-l].b_delta, self[-l-1].acts.T)

        # update weights and biases
        for l in range(1, self.layer_count):
            if self.optimizer == 'Gradient':
                self[l-1].weights -= self.gradient_descent(self[l].w_delta, learning_rate)
                self[l-1].biases -= self.gradient_descent(self[l].b_delta, learning_rate)

            elif self.optimizer == 'Adam':
                self[l-1].weights -= self.adam.update_weights(self[l].w_delta, layer_number=l-1, learning_rate=learning_rate)
                self[l-1].biases -= self.adam.update_biases(self[l].b_delta, layer_number=l-1, learning_rate=learning_rate)

            elif self.optimizer == 'Momentum':
                self[l-1].weights -= self.momentum.update_weight(self[l].w_delta, layer_number=l-1, learning_rate=learning_rate)
                self[l-1].biases -= self.momentum.update_biases(self[l].b_delta, layer_number=l-1, learning_rate=learning_rate)

    def get_cost(self, expected):
        return self.f_loss(expected, self[-1].acts)

    def epoch_cost(self, count):
        return self.cost_sum / count


    def gradient_descent(self, gradient_object, learning_rate=0.01):
        """
            Do Gradient Descent optimization
            :param gradient_object: gradient of weights/biases on prefered layer
            :param learining_rate: hiperparameter
        """
        return gradient_object * learning_rate


class Adam:
    """
        Do Adam optimization
        :param layers_count: number of layers in neural network
        :param gradient_weights/gradient_biases: gradient of weights/biases 
        :param learining_rate: hiperparameter
        :param beta1: hiperparameter
        :param beta2: hiperparameter
        :param epsilon: hiperparameter
    """
    def __init__(self, layers_count, beta1=0.9, beta2=0.999, epsilon=1e-8):
        self.learning_rate = None
        self.beta1 = beta1
        self.beta2 = beta2
        self.epsilon = epsilon
        self.m_w = {}
        self.v_w = {}
        self.t_w = {}
        self.m_b = {}
        self.v_b = {}
        self.t_b = {}
        for i in range(layers_count):
            self.m_w[i] = 0
            self.v_w[i] = 0
            self.t_w[i] = 0
            self.m_b[i] = 0
            self.v_b[i] = 0
            self.t_b[i] = 0
        
    def update_weights(self, gradient_weights, layer_number, learning_rate=0.001):
        self.t_w[layer_number] = self.t_w[layer_number] + 1

        self.m_w[layer_number] = self.beta1*self.m_w[layer_number] + (1 - self.beta1)*gradient_weights
        self.v_w[layer_number] = self.beta2*self.v_w[layer_number] + (1 - self.beta2)*(gradient_weights**2)

        m_hat = self.m_w[layer_number]/(1 - self.beta1**self.t_w[layer_number])
        v_hat = self.v_w[layer_number]/(1 - self.beta2**self.t_w[layer_number])

        return learning_rate*(m_hat/(np.sqrt(v_hat) + self.epsilon))

    def update_biases(self, gradient_biases, layer_number, learning_rate=0.001):
        self.t_b[layer_number] = self.t_b[layer_number] + 1

        self.m_b[layer_number] = self.beta1*self.m_b[layer_number] + (1 - self.beta1)*gradient_biases
        self.v_b[layer_number] = self.beta2*self.v_b[layer_number] + (1 - self.beta2)*(gradient_biases**2)

        m_hat = self.m_b[layer_number]/(1 - self.beta1**self.t_b[layer_number])
        v_hat = self.v_b[layer_number]/(1 - self.beta2**self.t_b[layer_number])

        return learning_rate*(m_hat/(np.sqrt(v_hat) + self.epsilon))



class Momentum:
    def __init__(self, layers_count, momentum=0.9):
        """
        Do Momentum optimization
        :param layers_count: number of layers in neural network
        :param gradient_weights/gradient_biases: gradient of weights/biases 
        :param learining_rate: hiperparameter
        :param momentum: hiperparameter
        """
        self.learning_rate = None
        self.momentum = momentum
        self.v_w = {}
        self.v_b = {}
        for i in range(layers_count):
            self.v_w[i] = 0
            self.v_b[i] = 0
        
    def update_weight(self, gradient_weights, layer_number, learning_rate=0.001):
        self.v_w[layer_number] = self.momentum*self.v_w[layer_number] + learning_rate * gradient_weights 
        return self.v_w[layer_number]

    def update_biases(self, gradient_biases, layer_number, learning_rate=0.001):
        self.v_b[layer_number] = self.momentum*self.v_b[layer_number] + learning_rate * gradient_biases 
        return self.v_b[layer_number]