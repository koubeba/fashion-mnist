import argparse
from neuralmath import neuralmath
from trainer import trainer, tester

# Parse the arguments

parser = argparse.ArgumentParser(description='Initialize a neural network with training data fetched from given path')

parser.add_argument('--path', required=True, help='path to training dataset')
parser.add_argument('--validate', help='run program in 5-fold validation run', metavar='True/False')
parser.add_argument('--test', help='test neural network data from specified file in output flag', metavar='True/False')

parser.add_argument('--output', default='nninfo.npz', help='output filename for trained nn')
parser.add_argument('--layer_sizes', type=int, nargs='+', help='list of layer sizes')
parser.add_argument('--activation_function', choices=['linear', 'sigmoid', 'relu', 'tanh'],
                    help='activation function of neurons in hidden layers', metavar='[linear, sigmoid, relu, tanh]')
parser.add_argument('--loss_function', choices=['L2'], help='loss function', metavar='[L2]')
parser.add_argument('--optimizer', choices=['Gradient', 'Adam', 'Momentum'], help='optimizer', metavar='[Gradient, Adam, Momentum]')
parser.add_argument('--epochs_count', type=int, choices=range(1, 10000),
                    help='number of epochs of training the network', metavar='[1, 10000)')
parser.add_argument('--epochs_debug', type=int, choices=range(1, 10000),
                    help='number of epochs after which debug information is printed', metavar='[1, 10000)')
parser.add_argument('--learning_rate', type=float, help='learning rate', metavar='(0, 1)')
parser.add_argument('--plot', type=int, help='visible plot', metavar='[1, 0]')

args = parser.parse_args()

if args.test and args.validate:
    print('cannot test and validate in the same time')
    exit()

if args.test:
    te = tester.Tester(args.path, args.output)
    te.init_neural_network()
    te.validate()
    exit()

# Initialize a trainer with provided parameters
if len(args.layer_sizes) <= 2:
    print('Layer must have at least one hidden layer!')
    exit()

if args.layer_sizes[0] != 784:
    print('Input size must be 784')
    exit()

if args.layer_sizes[-1] != 10:
    print('Output size must be 10')
    exit()

if args.output and not str(args.output).endswith('.npz'):
    print('output file must end with .npz extension')
    exit()

tr = trainer.Trainer(args.path, args.output, args.layer_sizes)

if args.activation_function:
    if args.activation_function == 'linear':
        tr.set_activation_function(neuralmath.linear)
        tr.set_activation_function_prime(neuralmath.linear_prime)
    elif args.activation_function == 'sigmoid':
        tr.set_activation_function(neuralmath.sigmoid)
        tr.set_activation_function_prime(neuralmath.sigmoid_prime)
    elif args.activation_function == 'relu':
        tr.set_activation_function(neuralmath.reLU)
        tr.set_activation_function_prime(neuralmath.reLU_prime)
    elif args.activation_function == 'tanh':
        tr.set_activation_function(neuralmath.tanh)
        tr.set_activation_function_prime(neuralmath.tanh_prime)

if args.loss_function:
    if args.loss_function == 'L2':
        tr.set_loss_function(neuralmath.L2_loss)
        tr.set_loss_function_prime(neuralmath.L2_loss_prime)

if args.optimizer:
    tr.set_optimizer(args.optimizer)

if args.epochs_count:
    tr.set_epochs_count(args.epochs_count)

if args.epochs_debug:
    tr.set_epochs_debug(args.epochs_debug)

if args.learning_rate:
    tr.set_learning_rate(args.learning_rate)

if args.plot == 0 or args.plot == 1:
    tr.set_plot(args.plot)

tr.init_neural_network()
if args.validate:
    tr.k_fold(5)
else:
    tr.train_network()
    tr.save_nn_info(args.output)
    te = tester.Tester(args.path, args.output)
    te.init_neural_network()
    te.validate()
