from neuralmath import neuralmath
from neural import neural
from utils import read_data
import numpy


class Tester:
    '''
    Class defining Tester object used for checking results of NN
    after loading trained NN data 
    '''
    def __init__(self, path, outputpath):
        '''
        Initialization function of Tester object

        Imports Fashion MNIST dataset
        And loads important NN information from given file

        :param path: Path to MNIST data
        :param outputpath: Path to trained NN output data
        '''

        # Input and output, from the training sets
        self.__dataset = read_data.DataSet(path = path, datatype = 't10k')
        self.__input = self.__dataset.get_image_vector()/255
        self.__output = numpy.reshape(self.__dataset.get_Y_matrix(), (-1, 10))
        self.__labels = self.__dataset.Y
        self.__layers = None

        # Default neural network parameter
        self.load(outputpath)

        self.__neural_network = None

    def init_neural_network(self):
        '''
        Initialization of NN for testing

        Uses __init__() of NeuralNetwork class to assign
        new object to ``__neural_network`` field
        '''
        self.__neural_network = neural.NeuralNetwork(
                    layer_sizes=self.__layer_sizes,
                    f_activ=self.__activation_function,
                    f_activ_prime=self.__activation_function_prime,
                    f_loss=self.__loss_function,
                    f_loss_prime=self.__loss_function_prime,
                    layers=self.__layers,
                    optimizer=self.__optimizer)

    def load(self, filename='nninfo.npz'):
        '''
        Function used for loading trained NN output data

        Import ``Layer`` objects and every single essential info used in creation of
        new Neural Network from trained data

        :param filename: name of imported file
        '''
        import os
        npz = numpy.load(os.path.join(os.curdir, filename))

        self.__layers = list(npz['layers'])
        self.__activation_function, self.__activation_function_prime = npz['activation_funcs'][0]
        # fix for old trained data before change
        if not 'loss_funcs' in npz.keys():
            self.__loss_function = neuralmath.L2_loss
            self.__loss_function_prime = neuralmath.L2_loss_prime
        else:
            self.__loss_function, self.__loss_function_prime = npz['loss_funcs'][0]
        self.__layer_sizes = list(npz['layer_sizes'])
        if not 'loss_funcs' in npz.keys():
            self.__optimizer = 'Gradient'
        else:
            self.__optimizer = npz['optimizer']
        

    def validate(self):
        '''
        Function used for validating results of NN

        Uses feeding forward of NN and checks whether output is closest
        to expected output and prints the result
        '''
        correct = [0 for x in range(10)]
        corr = 0
        alls = 0
        print('\n')
        for i in range(self.__output.shape[0]):
            alls += 1
            val = self.predict(self.__input[i])
            if (val == self.__labels[i]):
                corr += 1
            print('%d' % corr + ' / %d' % alls, end='\r')
        print('\n')

    def predict(self, inp):
        '''
        Check if given input is predicted as expected

        :param inp: test image data
        :return: vector with only one elem set as 1 (the maximum one) 
                and rest zeroed
        '''
        inp = inp.reshape(self.__neural_network[0].acts.shape)

        self.__neural_network.feed_forward(inp)
        return numpy.argmax(self.__neural_network[-1].acts)
