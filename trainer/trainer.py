from neuralmath import neuralmath
from neural import neural
from utils import read_data
import numpy
import matplotlib.pyplot as plt

class Trainer:
    '''
    Class Trainer that is used for training NN
    '''
    def __init__(self, path, output_path, layer_sizes):
        '''
        Initialization function of Trainer object

        :param path: path to Fashion MNIST data
        :param output_path: name/path_to_file to save trained nn data
        :param layer_sizes: list of layer neuron sizes, eg. [784, 20, 10]
        '''

        # Input and output, from the training sets
        self.__dataset = read_data.DataSet(path = path, datatype = 'train')
        self.__input = self.__dataset.get_image_vector()/255
        self.__output = numpy.reshape(self.__dataset.get_Y_matrix(), (-1, 10))
        self.__output_path = output_path

        # Default neural network parameters

        self.__activation_function = neuralmath.reLU
        self.__activation_function_prime = neuralmath.reLU_prime
        self.__loss_function = neuralmath.L2_loss
        self.__loss_function_prime = neuralmath.L2_loss_prime
        self.__optimizer = 'Gradient'
        self.__layer_sizes = layer_sizes
        self.__neural_network = None
        self.__plot = True

        # Default training parameters
        self.__epochs_count = 10
        self.__epochs_debug = 1
        self.__learning_rate = 0.0007
        pass

    def set_activation_function(self, function):
        self.__activation_function = function

    def set_activation_function_prime(self, function):
        self.__activation_function_prime = function

    def set_loss_function(self, function):
        self.__loss_function = function

    def set_loss_function_prime(self, function):
        self.__loss_function_prime = function

    def set_epochs_count(self, epochs):
        self.__epochs_count = epochs

    def set_epochs_debug(self, debug):
        self.__epochs_debug = debug

    def set_learning_rate(self, rate):
        self.__learning_rate = rate

    def set_optimizer(self, optimizer):
        self.__optimizer = optimizer
    
    def set_plot(self, plot):
        self.__plot = plot

    def init_neural_network(self):
        '''
        Initialization of NN for training

        Uses __init__() of NeuralNetwork class to assign
        new object to ``__neural_network`` field
        '''

        self.__neural_network = neural.NeuralNetwork(
                    layer_sizes=self.__layer_sizes,
                    f_activ=self.__activation_function,
                    f_activ_prime=self.__activation_function_prime,
                    f_loss=self.__loss_function,
                    f_loss_prime=self.__loss_function_prime,
                    optimizer=self.__optimizer)

    def k_fold(self, k=1):
        '''
        K-Fold Cross Validation Function

        Splits training set into same-sized blocks of data and every
        iteration one (other than previous ones) block is used as validation
        set and the rest is training

        After k iterations it stops and calculate average error

        Used for validating NN efficiency

        :param k: how many blocks to create from training set
        '''

        if not isinstance(k, int):
            print('K must be integer')
            exit()

        if self.__input.shape[0] % k != 0:
            print('K must be multiplier of training sets size')
            exit()

        elem_count = int(self.__input.shape[0])
        block_size = int(elem_count / k)
        cost_functions = []
        
        for i in range(k):
            self.__neural_network = None
            self.init_neural_network()

            print('\n========== FOLD: %d ==========\n' % i)
            begin = i * block_size
            end = begin + block_size
            validation_set = self.__input[begin : end].\
                        reshape(-1, 784)
            validation_outputs = self.__output[begin : end].\
                        reshape(-1, 10)
            training_set = numpy.concatenate((self.__input[0 : begin], \
                        self.__input[end : elem_count])).\
                        reshape(-1, 784)
            training_outputs = numpy.concatenate((self.__output[0 : begin], \
                        self.__output[end : elem_count])).\
                        reshape(-1, 10)
            trained = 0
            for epoch in range(self.__epochs_count):
                #perm0 = numpy.arange(training_set.shape[0])
                #numpy.random.shuffle(perm0)
                #training_set = training_set[perm0]
                #training_outputs = training_outputs[perm0]

                for elem in range(training_set.shape[0]):
                    self.__neural_network.feed_forward(training_set[elem])
                    self.__neural_network.backpropagate(self.__learning_rate,
                                        training_outputs[elem])
                    trained += 1
                    print('Epoch %d: %s / %d' % (epoch, elem, training_set.shape[0]), end='\r')
                
                print('\n')
            
            print('\nCost function value: %.6f' % \
                    self.__neural_network.epoch_cost(trained))
            cost_functions.append(self.__neural_network.epoch_cost(trained))
            trained = 0
            self.__neural_network.cost_sum = 0

        print('\nFinal error: %d' % str(sum(cost_functions)/k))
            

            

    def train_network(self):
        '''
        Main function used for training of Neural Network

        Loops for certain amount of epochs and
        feed forwards and backpropagate for every single image
        which leads to better and better predictions of NN
        '''

        cost_functions = []
        trained = 0

        # main loop going one input image by one in training
        # for epoch
        for i in range(self.__epochs_count):

            # shuffle training set
            perm0 = numpy.arange(self.__input.shape[0])
            numpy.random.shuffle(perm0)
            self.__input = self.__input[perm0]
            self.__output = self.__output[perm0]

            # for image
            for j in range(self.__input.shape[0]):
                self.__neural_network.feed_forward(self.__input[j])
                self.__neural_network.backpropagate(self.__learning_rate,
                                    self.__output[j])
                trained += 1
                print('%s / 60000' % j, end='\r')

            print('\n')
                       

            # if we made certain amount of iteration we should calculate cost function
            if i % self.__epochs_debug == 0:
                print('\nCost function value: %.6f' % \
                self.__neural_network.epoch_cost(trained), end='\n')
                cost_functions.append(self.__neural_network.epoch_cost(trained))
                trained = 0
                self.__neural_network.cost_sum = 0

            print(i, end='\n')

        # log every epoch cost function value into file
        with open(self.__output_path + '.log', 'w+') as f:
            iter = 0
            for cost_function in cost_functions:
                iter += 1
                f.write(str(iter) + ': ' + str(cost_function) + '\n')

        # print plot if activated
        if self.__plot:
            plt.plot(cost_functions)
            plt.show()
        
    def save_nn_info(self, filename='nninfo.npz'):
        '''
        Function used for saving trained NN data to use later in training

        :param filename: name of file to save NN data to
        '''
        import os

        activation_funcs = []
        loss_funcs = []

        activation_funcs.append((self.__neural_network.f_activ, \
                    self.__neural_network.f_activ_prime))
        loss_funcs.append((self.__loss_function, self.__loss_function_prime))

        numpy.savez_compressed(
            file=os.path.join(os.curdir, filename),
            layers=self.__neural_network.layers,
            activation_funcs=activation_funcs,
            loss_funcs=loss_funcs,
            optimizer=self.__optimizer,
            layer_sizes=self.__layer_sizes
        )
