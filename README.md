# Fashion MNIST

Problem, który stworzona przez nas sieć neuronowa ma rozwiązać, to klasyfikacja obrazów do jednej z dziesięciu kategorii. W tym celu stworzyliśmy jednokierunkową, wielowarstwową sieć w pełni połączoną, której hiper parametry mogą być zmieniane.

## Wczytywanie danych

#### Informacje o danych Fashion MNIST
Dane, które będziemy pobierać w ramach trenowania i testowania naszej sieci neuronowej są spakowane w formacie gzip. Python podstawowo posiada narzędzie (moduł gzip), które umożliwi nam odczytanie wymaganych danych.

#### load_data() - Wyciąganie danych z plików .gz
Aby wyciągnąć dane wykorzystujemy 2 moduły: gzip oraz numpy.
Dzięki gzip możemy otworzyć plik .gz i przestrumieniować jego zawartość jak przy odczytu zwykłego pliku.
Później dzięki numpy i strumieniu danych wytworzonych przez gzip tworzymy wektory danych.

Aby wyciągnąć informacje o label'ach wykorzystujemy funkcję:
labels = np.frombuffer(lbpath.read(), dtype=np.uint8, offset=8)
Z dokumentacji wiemy, że każdy pojedynczy label jest zmieszczony w bajcie i pierwszy label zaczyna się dopiero po 8 bajtach strumienia.

Aby wyciągnąć informacje o obrazkach wykorzystujemy funkcję:
images = np.frombuffer(imgpath.read(), dtype=np.uint8, offset=16).reshape(len(labels), 784)
Z dokumentacji wiemy, że informacje o pikselach zaczynają się po 16-stym bajcie, a każdy piksel jest reprezentowany jako bajt zakodowany jako RGB.
Dodatkowo wiemy, że obrazki są rozmiarów 28x28, co po przemnożeniu daje nam 784 bajty. Dlatego dodaliśmy kolejny człon ".reshape", dzięki której rozdzielimy obrazki do oddzielnych wektorów.

#### create_sprite_image() - Utworzenie sprite'a do eksportu obrazu
Aby móc wyeksportować cały obraz w postaci zwartego kwadratu, spróbujemy przekształcić naszą tablicę obrazków na kwadratową macierz. Dzięki temu otrzymamy bardziej skondensowany wynikowy obraz wszystkich obrazków. 

Np. jak mamy wektor 200 obrazków, to naszym celem jest utworzenie z nich macierzy kwadratowej.
Aby to osiągnąć musimy minimalny rozmiar boku macierzy, który pomieści wszystkie te obrazki.
Otrzymujemy to dzięki:

Czyli w tym przypadku rozmiarem boku macierzy będzie 15, bo 15*15 =225, a 14*14 = 196 < 200.

#### get_sprite_image() - Zwrócenie poprawnego sprite'a
Ta funkcja służy głównie tylko po to by:
a) przemienić wektor ciągłych wektorów danych na wektor macierzy
b) jeżeli to było ustalone, odwrócić RGB
c) utworzyć wskazany sprite i go zwrócić

#### class DataSet - interface dostępu do danych
Zawiera takie pola jak:
datatype - typ danych (dostępne dla Fashion MNIST jest: train, t10k)
X - wektor ciągłych wektorów danych o obrazkach (nieodwrócony w RGB)
Y - wektor bajtów, które określają labele każdego kolejnego elementu
X_sprite - kwadratowa macierz obrazków (odwrócone w RGB (zmienić?) )
X_sprite_plots - rozmiar ściany X_sprite

#### Jak dane są przechowywane w Trainerze
Input: Macierz rozmiaru liczba zdjęć x 784, gdzie w ramach trainera liczba zdjęć == 60000.
Output: Macierz rozmiaru liczba zdjęć x 10, gdzie w ramach trainera liczba zdjęć == 60000.
Ale do sieci neuronowej wartości pikseli jak i oczekiwany output podawane są pojedynczo, tj. jako pojedyncze wektory.

# Funkcje aktywacji

## Funkcja liniowa

![liniowa-1545821962.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/101689/liniowa-1545821962.png)

### Zalety

- Daje zakres aktywacji (niebinarna aktywacja)
- We can definitely connect a few neurons together and if more than 1 fires, we could take the max ( or softmax) and decide based on that.

### Wady

- Stała pochodna, a co za tym idzie, griadent, powoduje, że zmiany będące wynikiem propagacji wstecznej są stałe i nie zależą od zmiany w wejściu X.

## Funkcja sigmoidalna

![sigmoid-1545822529.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/101689/sigmoid-1545822529.png)

### Zalety

- Funkcja nieliniowa- przy czym złożenie funkcji sigmoidalnych także będzie nieliniowe.
- Analogowa aktywacja.
- Gradient także jest nieliniowy.
- Dobra funkcja do użycia w klasyfikatorach.
- Wartości w zakresie (0, 1). (Nie grozi eksplozją gradientu)

### Wady

- W pobliżach asymptot wartości coraz mniej reagują na zmiany wejścia X.
- Możliwy problem zanikającego griadentu. (Zmiany w wagach będą stawać się zbyt małe, żeby sieć się uczyła)

## Funkcja reLU

![relu-1545822545.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/101689/relu-1545822545.png)

### Zalety

- Nie jest podatny na problem zanikającego griadentu.
- ReLU jest mało kosztowne obliczeniowo w porównaniu z innymi funkcjami aktywacyjnymi.

### Wady

- Funkcja powinna być używana tylko w przypadku neuronów warstw ukrytych.
- W przypadku aktywacji dla x<0, gradient będzie zerowy, co oznacza, że wagi nie będą aktualizowane. Neurony przestaną odpowiadać na zmiany (problem umierającego ReLU).
- Wartości ReLu są w zakresie \[0, inf). To znaczy, że gradienty mogą szybko stać się zbyt duże.

# Pochodne funkcji aktywacji

Pochodne funkcji względem argumentu wejściowego (będącym argumentem wyjściowym poprzedniej warstwy). Używane we wstecznej propagacji.

# Funkcje straty

## Funcja L2 (MSE- Mean squared error)

![L2-1545821653.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/101689/L2-1545821653.png)

# Zamiana reLU

W commicie *\#c62f12d8* mieliśmy nieefektywne reLU, które wykonywało się w tempie 6s dla 5000x5000 tablicy.

Poprawiliśmy wynik w commicie *\#e90d4786* stosując maskowanie listy w numpy, które znacząco przyśpieszyło działanie algorytmu relu.  
Porównanie dla zwykłych funkcji:

![image-1545763383.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/105895/image-1545763383.png)

Porównanie dla funkcji pochodnych

![image-1545763179.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/105895/image-1545763179.png)

Klasy wykorzystane do modelowania sieci neuronowej

# Layer

Warstwa neuronów w sieci.

## Atrybuty klasy 

| nazwa pola | opis |
| ------ | ------ |
| neuron_count | Liczba neuronów w sieci |
| b_delta | Wektor wartości funkcji straty dla kolejnych wag warstw | 
| w_delta | Wektor wartości funkcji straty dla kolejnych wag warstw |
| zs | Wektor wartości obliczonych w sieci |
| acts | Wektor aktywacji, czyli wartości, które są wyjściem neuronów |
| weights | Wektor wag, które wskazują na aktualną warstwę. Dla i-tej warstwy w weights zawarte są połączenia między i-1-tą warstwą a i-tą. |
| biases | Wektor biasów, które wskazują na aktualną warstwę. Dla i-tej warstwy w biases zawarte są połączenia między i-1-tą warstwą a i-tą. |


## Konstruktory klasy

| nazwa | opis |
| ------ | ------ |
| Layer | konstruktor podstawowy, przyjmujący parametry layer_count i next_layer_count |

# NeuralNetwork

Klasa implementująca sieć neuronową o różnej liczbie warstw ukrytych.

## Atrybuty klasy

| nazwa pola | opis |
| ------ | ------ |
| cost_sum | Suma kosztów danych wejściowych, która jest wykorzystywana przez Trainera później do liczenia uśrednionego błędu dla całej epoki | |
| layers | Wektor zawierający odniesienia do konkretnych warstw sieci | 
| layer_sizes | Wektor liczb neuronów w każdej kolejnej warstwie. Np. [784, 20, 20, 10] utworzy sieć neuronową stworzoną z warstw zawierających kolejno 784, 20, 20, 10 neuronów. |
| layer_count | Liczba warstw sieci |
| f_activ | Funkcja aktywacji neuronów w warstwach ukrytych |
| f_activ_prime | Pierwsza pochodna funkcji aktywacji neuronów względem argumentu wejściowego neuronów |
| f_loss | Funkcja straty |
| f_loss_prime | Pierwsza pochodna funkcji straty względem argumentów wejściowych |

## Konstruktory klasy

| nazwa | opis |
| ------ | ------ |
| NeuralNetwork | Konstruktor podstawowy klasy. Przyjmuje parametry: layer_sizes, f_activ, f_activ_prime, f_loss, f_loss_prime, layers |

## Operatory klasy

| nazwa | opis |
| ------ | ------ |
| [n] | Zwraca n-tą warstwę neuronów (jako obiekt **Layer**) |

## Metody klasy

| nazwa | opis |
| ------ | ------ |
| feedforward() | Wykonuje propagację wartości w sieci |
| backpropagate(*learning_rate*) | Wykonuje propagację wsteczną i aktualizację wartości wag w sieci zgodnie z zadanym parametrem *learning_rate* | 
| get_cost(*expected*) | Zwraca wartość funkcji straty |
| epoch_cost(*count*) | Zwraca sumę kosztów dzieloną na ilość elementów treningowych |
| gradient_descent(*gradient_object*, *learning_rate=0.01*) | Wykonuje metodę gradientu prostego z użyciem wag/biasów, gradientów obliczonych dla wag/biasów oraz learning_rate |

#### Metoda propagacji wstecznej

θ=θ−η⋅∇J(θ)

θ - weights_biases
η - learning_rate
∇J(θ) - gradient_object

### Klasa Adam

*  **__init__**(self, layers_count, beta1=0.9, beta2=0.999, epsilon=1e-8)

*  **update_weights**(self, gradient_weights, layer_number, learning_rate=0.001)

Wykonuje metodę adam-a z użyciem wag/biasów, gradientów obliczonych dla wag/biasów oraz learning_rate oraz hiperparametrów:
* beta1 = 0.9
* beta2 = 0.999 
* epsilon = 1e-8 

Wartości zostały dobrane jako powszechnie używane w znanych bibliotekach ML

*  **update_biases**(self, gradient_biases, layer_number, learning_rate=0.001)

E[x^n] - jest to moment n tego stopnia, zdefiniowany jako wartość spodziewana zmiennej x podniesionej do n-tej potęgi 
Najpierw obliczamy średnie ruchome

![a1.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/105960/obraz-1547206161.png)

Chcemy aby średnie ruchome oszacowywały pierwszą i drugą potęgę gradientu:

![a2.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/105960/obraz-1547206376.png)

W tym celu musimy poprawić oszacowanie:

![a3.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/105960/obraz-1547206570.png)

Oraz zaktualizować parametry:

![a4.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/105960/obraz-1547206601.png)

### Klasa Momentum

*  **__init__**(self, layers_count, momentum=0.9)

*  **update_weight**(self, gradient_weights, layer_number, learning_rate=0.001)

Wykonuje metodę momentum-a z użyciem wag/biasów, gradientów obliczonych dla wag/biasów oraz learning_rate oraz hiperparametrów:

*  momentum = 0.9

Wartości zostały dobrane jako powszechnie używane w znanych bibliotekach ML

*  **update_biases**(self, gradient_biases, layer_number, learning_rate=0.001)

Momentum dodaje wpływ poprzedniego kroku
Wykonujemy następujące operacje:

![m1.png](https://tettra-production.s3.us-west-2.amazonaws.com/teams/40908/users/105960/obraz-1547239538.png)

* θ - weights_biases
* η - learning_rate
* ∇J(θ) - gradient_object
* vt - current step vector
* vt-1 - last step vector
* y - momentum

# Klasa trenera

Trener tworzy obiekt klasy sieci neuronowej z zadanymi parametrami i trenuje ją, wykorzystując do tego wskazane dane treningowe.

## Atrybuty klasy

| nazwa | opis |
| ------ | ------ |
| dataset | obiekt klasy Dataset, zbiór danych |
| input | wektor danych wejściowych, uzyskany z obiektu Dataset | 
| output | wektor spodziewanych danych wyjściowych, uzyskany z obiektu Dataset |
| layer_sizes | lista zawierająca liczby neuronów we wszystkich warstwach. Np. [784, 20, 10] |
| activation_function | funkcja aktywacji, używana w warstwach ukrytych |
| activation_function_prime | pierwsza pochodna funkcji aktywacji względem danych wejściowych, używanej w warstwach ukrytych |
| loss_function | funkcja kosztu |
| loss_function_prime | pierwsza pochodna funkcji kosztu względem danych wejściowych |
| neural_network | obiekt klasy Neural Network |
| epochs_count | liczba epok trenowania sieci |
| epochs_debug | liczba epok, po której będą wyświetlane dane debugowe (status trenowania, obecna wartość funkcji kosztu...) |
| learning_rate | współczynnik uczenia (przemnożony przez gradient przy propagacji wstecznej) |
| optimizer | wskazanie na klase Optimizer, która będzie wykorzystywana podczas treningu |
| plot | wartość boolean wskazująca czy ma się wyświetlić wykres czy nie |


## Konstruktor domyślny

konstruktor podstawowy, przyjmujący parametry:
* path- ścieżka danych trenujących
* parametry inicjalizowane są wartościami domyślnymi.
* output_path - nazwa pliku, do którego zapiszemy najwazniejsze informacje o sieci neuronowej
* layer_sizes - lista wielkości kolejnych warstw, np [784, 20, 10]

## Wartości domyślne parametrów sieci neuronowej

| parametr | wartość |
| ------ | ------ |
| activation_function | reLU |
| loss_function | L2 | 

## Wartości domyślne trenera

| parametr | wartość |
| ------ | ------ |
| epochs_count | 10 |
| epochs_debug | 1 | 
| learning_rate | 0.0007 |

## Metody trenera

*  **train_network** 

Trenuje sieć neuronową przez epochs_count epok (wywołując w każdej epoce metody sieci neuronowej: feedworward, backpropagate), co epochs_debug wypisując dane debugowe

*  **k_fold**

Funkcja służąca do walidacji efektywności wskazanej sieci neuronowej. Polega na:
Podziale treningowego zbioru danych na k bloków równego rozmiaru. 
Z tych bloków wybieramy jeden i używamy go jako zbiór walidacyjny oraz resztę jako treningowy. Badamy efektywność treningu
Bierzemy kolejny blok jako walidacyjny oraz resztę jako treningowy i powtarzamy procedurę k razy.

*  **save_nn_info**

Funkcja służąca do zapisu najważniejszych danych z wytrenowanej sieci neuronowej

# Klasa testera

Tester jest klasą służącą do wykonania weryfikacji efektywności sieci, która wcześniej była wytrenowana. Liczy celność (tj. stosunek dobrze odgadniętych obrazków do wszystkich testowych)

## Atrybuty klasy

| nazwa | opis |
| ------ | ------ |
| dataset | obiekt klasy Dataset, zbiór danych |
| input | wektor danych wejściowych, uzyskany z obiektu Dataset | 
| output | wektor spodziewanych danych wyjściowych, uzyskany z obiektu Dataset |
| layer_sizes | lista zawierająca liczby neuronów we wszystkich warstwach. Np. [784, 20, 10] |
| activation_function | funkcja aktywacji, używana w warstwach ukrytych |
| activation_function_prime | pierwsza pochodna funkcji aktywacji względem danych wejściowych, używanej w warstwach ukrytych |
| loss_function | funkcja kosztu |
| loss_function_prime | pierwsza pochodna funkcji kosztu względem danych wejściowych |
| neural_network | obiekt klasy Neural Network |
| epochs_count | liczba epok trenowania sieci |
| epochs_debug | liczba epok, po której będą wyświetlane dane debugowe (status trenowania, obecna wartość funkcji kosztu...) |
| learning_rate | współczynnik uczenia (przemnożony przez gradient przy propagacji wstecznej) |
| optimizer | wskazanie na klase Optimizer, która będzie wykorzystywana podczas treningu |

## Konstruktor domyślny

konstruktor podstawowy, przyjmujący parametry:
*  path- ścieżka danych trenujących

parametry inicjalizowane są wartościami domyślnymi.
*  outputpath - nazwa pliku, z którego odczytamy najwazniejsze informacje o sieci neuronowej

## Metody testera

*  **predict**

Używając feed forward zdeklarowany w klasie Sieci Neuronowej testuje czy wynik przekazania danych z wejścia będzie równy oczekiwanemu wynikowi

*  **validate** 

Funkcja służąca do walidacji efektywności sieci neuronowej. Wykonując predict na każdym elemencie testowym oblicza celność, tj. stosunek odgadniętych poprawnie obrazków do sumy obrazków testowych

*  **load**

Funkcja służąca do odczytu najważniejszych danych z wytrenowanej sieci neuronowej

# Cross-Grid

Skrypt mający na celu znalezienie optymalnych parametrów dla sieci neuronowej poprzez testowanie różnych ustawień.

## Dane wejściowe:

**EPOCHS\_COUNT** - ilość epok przez które testujemy każde rozwiązanie

**EPOCHS\_DEBUG** - ilość epok po których dostajemy aktualne logi

**optimizers** - lista wszystkich zaimplementowanych optimizerów

**activations\_functions** - lista wszystkich zaimplementowanych funkcji aktywacji

**loss\_function** - lista wszystkich zaimplementowanych funkcji straty

**layers\_sizes** - lista przykładowych ustawień wielkości sieci

**learning\_rate** - lista przykładowych ustawień hiperparametru learning rate

(Linie 17 - 30)

W pierwszym etapie budujemy słownik zawierający połączenie poszczególnych parametrów - każdy z każdym

(Linie 31 - 56)

Budujemy komendę bashową dla każdego elementu z słownika, następnie ta komenda jest wywoływana przez moduł standardowy pythona subprocess a jego output pipeline jest przekierowany do zmiennej output, następnie zapisujemy otrzymany rezultat w liście results.

(Linie 57-60)

Wyszukujemy najbardziej optymalne rozwiązanie w liście results.

# Porównanie Sieci Neuronowych

Porównanie sieci neuronowych zostało spisane w zewnętrzym serwisie pod następującym linkiem:
https://app.tettra.co/teams/quasarcorp/pages/porownanie-sieci-neuronowych?auth=3c370982c25c4d2532a9caaf1cededd80fea54b4c6264c1065e5a67c45e4d00aef9c0243d28cdb6ca25e6c609a0b7d1a