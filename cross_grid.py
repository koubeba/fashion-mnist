# !/usr/bin/python
import subprocess

EPOCHS_COUNT = 3
EPOCHS_DEBUG = 1
PLOT = 1

optimizers = ['Gradient', 'Adam', 'Momentum']
activations_functions = ['sigmoid', 'relu', 'tanh']
loss_function = ['L2', 'L1']
layers_sizes = ['784 150 10', '784 70 30 10', '784 55 40 25 10']
learning_rate = ['0.1', '0.01', '0.001', '0.0001']

parametrs_dict = {}

# Counter of all possible params
i=0
for opt in optimizers:
    for act in activations_functions:
        for loss in loss_function:
            for layer in layers_sizes:
                for rate in learning_rate:
                    parametrs_dict[i] = {
                                        "optimizers" : opt,
                                        "activations_functions" : act,
                                        "loss_function" : loss,
                                        "layers_sizes" : layer,
                                        "learning_rate" : rate}
                    i += 1


results = []
for param in range(i):
    output=None
    command = ("python3 fashion-mnist.py --path ./data/ " \
                + "--layer_sizes {} --activation_function {} " \
                + "--loss_function {} --optimizer {} --learning_rate {} " \
                + "--epochs_count {} --epochs_debug {} --plot {} --output {}").format( \
                parametrs_dict[param]['layers_sizes'], \
                parametrs_dict[param]['activations_functions'], \
                parametrs_dict[param]['loss_function'], \
                parametrs_dict[param]['optimizers'], \
                parametrs_dict[param]['learning_rate'], \
                EPOCHS_COUNT, \
                EPOCHS_DEBUG, \
                PLOT, \
                'test_' + str(param) + '.npz')

    command = command.split()
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output, error = process.communicate()
    results.append(output.decode("utf-8")[-15:-11])
    print("Tested: LAYERS_SIZE |{}| ACTIVATION_FUNCTION |{}| LOSS_FUNCTION |{}| OPTIMIZER |{}| LEARNING_RATE |{}| \nWITH ACCURACY: {}/10000\n".format(\
                parametrs_dict[param]['layers_sizes'],\
                parametrs_dict[param]['activations_functions'], parametrs_dict[param]['loss_function'],
                parametrs_dict[param]['optimizers'], parametrs_dict[param]['learning_rate'], results[param]))

my_max = max(results)
my_index = results.index(my_max)
print("\nMost optimal configuration {} \nwith accuracy : {}/10000".format(\
        parametrs_dict[my_index], my_max))